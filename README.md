# SignBank

Documentation tool accompanying signs and their glosses for use by people who have Libras as their mother tongue and by a wide range of other users.

## Description

Documentation and monitoring tool for signs and their glosses for use by people who have Libras as their mother tongue and by a wide range of other users.
This tool has three types of users and mode of use. See the types: 1) Visitors: anyone who accesses the portal. This type of user will be able to use the portal to access the signal they are looking for,to facilitate this search, the user will have different ways of searching for a signal. 2) Poster: registered users will be able to access the tool and register these signals, they will have access to a specific administrative panel. 3) Administrators: they will have access to all information on the portal and the possibility to change data on the registration of signals, portal identity, among other things.

## Project status

Under construction.

## Installation

```
git clone https://gitlab.com/levantelab-all/signbank.git
```

---

## License

For open source projects, say how it is licensed.

## Authors

Levante Lab's developer team: http://levantelab.com.br/.

## Readme Details

This 'readme' was built following the best practices suggested by [makeareadme.com](https://www.makeareadme.com/).
